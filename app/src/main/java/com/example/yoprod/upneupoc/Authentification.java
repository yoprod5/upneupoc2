package com.example.yoprod.upneupoc;

import android.Manifest;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;



import com.firebase.ui.auth.AuthUI;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;


import java.util.Collections;


public class Authentification extends AppCompatActivity implements View.OnClickListener {

    private static final int RC_SIGN_IN = 123;

    private FirebaseAuth mAuth;

   // private static final int RC_CHOOSE_PHOTO = 101;
    private TextView mStatusView;
    private TextView mDetailView;
    private ImageView imgProfile;
  //  private FloatingActionButton flouer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_authentification);

        // Initialize Firebase Auth
        mAuth = FirebaseAuth.getInstance();

        mStatusView = findViewById(R.id.txtEmail);
        mDetailView = findViewById(R.id.txtUser);
        imgProfile = findViewById(R.id.imgProfile);
      //  flouer = findViewById(R.id.floatingActionButton);
        findViewById(R.id.btnSignin).setOnClickListener(this);
        findViewById(R.id.btnSignout).setOnClickListener(this);
        //findViewById(R.id.btnprofil).setOnClickListener(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        updateUI(mAuth.getCurrentUser());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {

            if (resultCode == RESULT_OK) {
                // Sign in succeeded
                updateUI(mAuth.getCurrentUser());
                startActivity(new Intent(this, Authentification.class));
                finish();
                return;
            } else {
                // Sign in failed

                Toast.makeText(this, "Sign In Failed", Toast.LENGTH_SHORT).show();
                updateUI(null);
            }
        }
    }

    private void startSignIn() {

        Intent intent = AuthUI.getInstance().createSignInIntentBuilder()
                .setIsSmartLockEnabled(!BuildConfig.DEBUG)
                .setAvailableProviders(Collections.singletonList(
                        new AuthUI.IdpConfig.EmailBuilder().build()))
                .setLogo(R.drawable.logo)
                .build();

        startActivityForResult(intent, RC_SIGN_IN);
    }

    private void updateUI(FirebaseUser user) {
        if (user != null) {
            // Signed in
            mStatusView.setText(user.getEmail());
            mDetailView.setText(user.getDisplayName());
            imgProfile.setImageResource(R.drawable.profile);
          //  flouer.setImageResource(R.drawable.ic_input_add);
            findViewById(R.id.btnSignin).setVisibility(View.GONE);
            findViewById(R.id.btnSignout).setVisibility(View.VISIBLE);
          //  findViewById(R.id.btnprofil).setVisibility(View.VISIBLE);
        } else {
            // Signed out
            mStatusView.setText(null);
            mDetailView.setText(null);
            //flouer.setImageBitmap(null);
            imgProfile.setImageBitmap(null);
           // findViewById(R.id.btnprofil).setVisibility(View.GONE);
            findViewById(R.id.btnSignin).setVisibility(View.VISIBLE);
            findViewById(R.id.btnSignout).setVisibility(View.GONE);
        }
    }

    private void signOut() {
        AuthUI.getInstance().signOut(this);
        updateUI(null);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnSignin:
                startSignIn();
                break;
            case R.id.btnSignout:
                signOut();
                break;
           // case R.id.:
             //   choosePhoto();
               // break;
        }
    }


   // protected void choosePhoto() {

     //   Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
      //  startActivityForResult(i, RC_CHOOSE_PHOTO);
    //}


}



